<?php
include "config.php";
$container_for_auto_groups=array("Groups", "GAFE","Distribution_lists","Automatic");

//$container_for_auto_groups=array("Groups");
echo "Connecting to MySQL \n\r\n";
$conn = new mysqli($DBServer, $DBUser, $DBPassword,$DB);
if ($conn->connect_error) {
        $emailnotification="Connection to the MySQL failed\n". $conn->connect_error ."\n";
        echo $emailnotification;
        exit();
}
echo "Connected to MySQL\n\r\n";

// LDAP
include (dirname(__FILE__) . "/adLDAP/lib/adLDAP/adLDAP.php");
echo "Connecting to LDAP\n\r\n";
try {
        $adldap = new adLDAP\adLDAP($ldap_options);
}
catch (adLDAPException $e) {
        exit();
}

$students=db_load($conn,"students","full");
$blockedstudents=array();
foreach($students as $key=> $student){
	if ($student['username']=="") continue;
	if ($student['status']=="0") { $blockedstudents[$key]=$student['username']; continue; }
        if ($student['status']=="1") $adldap->user()->enable($student['username']);
	echo $key." ".$student['first_name']." ".$student['last_name']."\n";
	if (! $adldap->user()->modify($student['username'], array("firstname"=>utf8_decode($student['first_name']),"lastname"=>utf8_decode($student['last_name']),"email"=> $student['username']."@ishamburg.org","office" => $key))) {
		continue; //not to create a new user. It should be created via ADVC tool
		echo $student['username']." email wasn't updated. Let's try to create an account\n";
		$grade="Grade 00";
		if (in_array($student['current_grade'],array("P1","P2","P3"))) $grade="Grade 00";
		else {
			$grade=str_replace("Grade ","",$student['current_grade']);
			$grade=sprintf("%02d", $grade);
			$grade="Grade ".$grade;
			}
		$attributes=array(
			"username"=>$student['username'],
			"logon_name"=>$student['username']."@intern.ishamburg.org",
			"firstname"=>utf8_decode($student['first_name']),
			"surname"=>utf8_decode($student['last_name']),
			"email"=>$student['username']."@ishamburg.org",
			"container"=>array("ish","students",$grade),
			"enabled"=>1,
			"password"=>"lieuhoi45ufi34bfev"
			);
		if (! $adldap->user()->create($attributes)) echo "User ".$student['username']." hasn't been created \n";
		else echo "User ".$student['username']." has been created \n";
		}
	}

unset($conn);
unset($final_groups);
unset($groups);
echo "These users should be blocked:\n";
print_r($blockedstudents);

function checkGroup($adldap,$groupname){
	$tmp=$adldap->group()->info($groupname);
	if (isset($tmp[0] ) && is_array($tmp[0]) ) return true;
	else return false;
	}

function is_in_array($array, $key, $key_value){
      $within_array = false;
      foreach( $array as $k=>$v ){
        if( is_array($v) ){
            $within_array = is_in_array($v, $key, $key_value);
            if( $within_array == true ){
                break;
            }
        } else {
                if( $v == $key_value && $k == $key ){
                        $within_array = true;
                        break;
                }
        }
      }
      return $within_array;
}

?>
