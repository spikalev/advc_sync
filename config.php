<?php

include "passwords.php"

$DBServer="localhost"; // db server ip address or dns name
$DBUser="root"; // db user
$DBPassword=""; // db password
$DB=""; // database name
$ldap_options=array(
        "account_suffix"=>"", //account suffix like "@domain.dom"
        "base_dn"=>"", //base dn like "DC=domain,DC=dom"
        "ad_port"=>"636", //port. LDAPS port is 636
        "domain_controllers" => array (""), // array of controllers
        "admin_username" =>"r", //username with admin rights
        "admin_password" =>"", // password for this username
        "use_ssl" =>"true"
);

$source="staff";
if ( isset($argv[1]) ){
        if ( $argv[1] == "students" ) $source="students";
        elseif ( $argv[1] == "parents" ) $source = "parents";
        }

$recreate=false;
if (isset($argv[2]) ){
	if ( $argv[2] == "recreate" ) $recreate=true;
	}

function vc_load($url,$count){
        $i=1;
        $finalarray=array();
        $url=$url."?count=".$count;
        $studentscount=0;
        $loop=true;
        while ($loop) {
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $url."&page=".$i);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POST, 0);
                $out = curl_exec($curl);
                curl_close($curl);
                $array=json_decode($out,true);
                $finalarray=array_merge($finalarray,$array);
                $i++;
                if (count($array)<$count) {$loop=false;}
        }
        return $finalarray;
}


function db_load($conn,$group,$type=""){
	if ($group=="classes" || $group=="parents") $query="select * from ".$group;
	else {
		if ($type=="full") $query="select * from ".$group;
		else $query="select * from ".$group." where status='1'";
		}
	$query="select * from ".$group;
	//echo $query;
	$result = mysqli_query($conn,$query);
	$students=array();
	if (mysqli_num_rows($result) > 0) {
		while($row = mysqli_fetch_assoc($result)) {
			if (isset($row['username'])) $username=strtolower($row['username']);
			 if ($group=="enrollments") {
	        		$students[$row['enrollment_pk']]['enrollment_pk']=$row['enrollment_pk'];
				$students[$row['enrollment_pk']]['update_date']=$row['update_date'];
				}
			elseif ($group=="classes") {
                                $students[$row['class_pk']]['class_pk']=$row['class_pk'];
                                $students[$row['class_pk']]['update_date']=$row['update_date'];
				}
			else {
				if ($type=="full"){
					$students[$row['person_pk']]=$row;
					}
				else {
	                                $students[$row['person_pk']]['person_pk']=$row['person_pk'];
        	                        $students[$row['person_pk']]['update_date']=$row['update_date'];
					}
				}
			}
		}
	return $students;
}




function db_create_structure($conn,$group,$user){
	$query="CREATE TABLE ".$group." (";
	$columns="";
        foreach ($user as $key=>$value){
               // if (is_string($value)) $user[$key]=mysqli_real_escape_string($conn,$value);
		if ($columns != "") $columns.=",";
		if (in_array($key,array("class_pk","group_fk","course_id","teacher_fk","enrollment_pk","class_fk","student_fk","person_pk","household_fk","homeroom_teacher_fk","parent_4_fk","parent_1_fk","parent_2_fk","parent_3_fk"))) $columns.="`".$key."` int(11) NOT NULL DEFAULT '0'";
		elseif (in_array($key,array("update_date","birthday"))) $columns.="`".$key."` datetime NOT NULL DEFAULT '1970-01-01 00:00:00'";
                elseif (in_array($key,array("update_link","teachers","meeting_times"))) $columns.="`".$key."`  text(50000)";
		else $columns.="`".$key."`  text(50000)";
                }
	if ($group=="enrollments") $columns.=", PRIMARY KEY (`enrollment_pk`) ) ENGINE=InnoDB CHARSET=utf8";
	elseif ($group=="classes") $columns.=", PRIMARY KEY (`class_pk`) ) ENGINE=InnoDB CHARSET=utf8";
	elseif ($group=="students"||$group=="staff") $columns.=", status tinyint(1) default 1 , PRIMARY KEY (`person_pk`) ) ENGINE=InnoDB CHARSET=utf8";
	else $columns.=", PRIMARY KEY (`person_pk`) ) ENGINE=InnoDB CHARSET=utf8";
	$query.=$columns;
	mysqli_query($conn,"drop table $group");
	if (!$result = mysqli_query($conn,$query)) {
                        echo "Couldnt do query: $query \n";
                        }
}

function db_user_update($conn,$group,$user,$update=true){
	$update_date=date('Y-m-d H:i:s',strtotime($user['update_date']));
	if ($update){
		// get update_date from mysql and compare it with VC update_date. If not equal - delete and create again. Otherwise exit
		if ($group=="classes") $query="delete from ".$group." where class_pk='".$user['class_pk']."'";
		elseif ($group=="enrollments") $query="delete from ".$group." where enrollment_pk='".$user['enrollment_pk']."'";
		else $query="delete from ".$group." where person_pk='".$user['person_pk']."'";
		//echo $query."\n";
		if (!$result = mysqli_query($conn,$query)) {
			echo "Couldnt do query: $query \n";
			}
		}
	$where="";
	$values="";
	foreach ($user as $key=>$value){
		if (is_string($value)) $value=mysqli_real_escape_string($conn,$value);
		if ($where!="") $where.=",";
		if ($values!="") $values.=",";
		$where.=$key;
		if ($key=="birthday" && strlen($value)<4) $value="1970-01-01";
		if (in_array($key,array("update_date"))) $values.="'".date('Y-m-d H:i:s',strtotime($value))."'";
		elseif (is_array($value)) $values.="'".mysqli_real_escape_string($conn,serialize($value))."'";
//		elseif (in_array($key,array("profile_codes","roles","teachers","meeting_times"))) $values.="'".mysqli_real_escape_string($conn,serialize($value))."'";
		else $values.="'".$value."'";
		}
	$finalquery="insert into ".$group." (".$where.") VALUES (".$values.");";
	//echo $finalquery."\n";
        if (!$result = mysqli_query($conn,$finalquery)) {
                  echo "Couldnt do query: $finalquery \n";
		echo mysqli_error($conn)."\n";
		//print_r($user);
                  }
	return 1;
}


function get_classes($conn){
	$query="select c.class_pk,c.teachers,c.class_id,s.username from classes c left join staff s on (c.teacher_fk=s.person_pk) where c.primary_grade_level in ('Grade 10','Grade 6','Grade 7','Grade 8','Grade 9','Grade 11','Grade 12','Grade 5');";
        $query="select c.class_pk,c.teachers,c.class_id,s.username from classes c left join staff s on (c.teacher_fk=s.person_pk) where c.status = 'Active' and s.status='1'";
	//$query="select c.class_pk,c.teachers,c.class_id,s.username from classes c left join staff s on (c.teacher_fk=s.person_pk) where c.status = 'Active'";
        $result = mysqli_query($conn,$query);
        $classes=array();
        if (mysqli_num_rows($result) > 0) {
                while($row = mysqli_fetch_assoc($result)){
		$classes[$row['class_pk']]['class_pk']=$row['class_pk'];
		$classes[$row['class_pk']]['name']=$row['class_id'];
                $classes[$row['class_pk']]['teachers']=$row['teachers'];
                if (isset($row['username'])) $classes[$row['class_pk']][]=$row['username'];
		}
	}
	foreach ($classes as $key=>$value){
		$add=false;
		if (isset($value['teachers'])) { if (! $tmps=unserialize($value['teachers']) ) print_r($value['teachers']);}
		else $tmps=array();
		unset($classes[$key]['teachers']);
		foreach ($tmps as $tmp){
			if (isset($tmp['person_fk'])&&($tmp['person_fk'])&&($tmp['role']=="Primary Teacher")) $classes[$key]['teacher_ids'][]=$tmp['person_fk'];
			}
		unset($tmps);
		$where="";
		if (isset($classes[$key]['teacher_ids'])) {
			foreach ($classes[$key]['teacher_ids'] as $teacher) {
				if ($where!="") $where.=",";
				$where.="'".$teacher."'";
				}
			}
		if ($where!="") $query_teachers="select s.username,s.person_pk,'teacher' as current_grade from staff s where s.status='1' and s.person_pk in ( ".$where." )";
		//if ($where!="") $query_teachers="select s.username,s.person_pk,'teacher' as current_grade from staff s where s.person_pk in ( ".$where." )";
		$query_students="select s.username,s.person_pk,s.current_grade from students s left join enrollments e on (s.person_pk = e.student_fk) left join classes c on (e.class_fk=c.class_pk) where e.class_fk='".$key."' and e.currently_enrolled='1' and s.status=1";
		if ($where!="") $common_query=$query_teachers." UNION ALL ".$query_students;
		else $common_query=$query_students;
		$common_result = mysqli_query($conn,$common_query);
	        if (mysqli_num_rows($common_result) > 0) {
         	       while($common_row = mysqli_fetch_assoc($common_result)){
				if (in_array($common_row['current_grade'],array("Grade 12","Grade 6","Grade 7","Grade 8","Grade 9","Grade 10","Grade 11","Grade 5")))  $add = true;
				if (isset($common_row['username'])&& $common_row['username']!="") $classes[$key]['members'][$common_row['person_pk']]=$common_row['username'];
				}
			}
		unset($classes[$key]['teacher_ids']);
		if (!$add) unset($classes[$key]);
		}
	return $classes;
}

function block($conn,$source,$dbusers){
	foreach ($dbusers as $pk => $dbuser){
                if ($source=="classes") $query="delete from $source where class_pk='".$pk."'";
//		elseif ($source=="parents"||$source=="students") $query="update $source set status=0 where person_pk='".$pk."'";
                elseif ($source=="enrollments") $query="delete from $source where enrollment_pk='".$pk."'";
                else $query="update $source set status='0' where person_pk='".$pk."'";
		//echo $query."\n";
		if (mysqli_query($conn,$query)) echo "PK $pk blocked\n";
		else echo "PK $pk cannot be blocked\n";
	}
}

?>


