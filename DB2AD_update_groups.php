<?php
include "config.php";
$container_for_auto_groups=array("Groups", "GAFE","Distribution_lists","Automatic");

echo "Connecting to MySQL \n\r\n";
$conn = new mysqli($DBServer, $DBUser, $DBPassword,$DB);
if ($conn->connect_error) {
        $emailnotification="Connection to the MySQL failed\n". $conn->connect_error ."\n";
        echo $emailnotification;
        exit();
}
echo "Connected to MySQL\n\r\n";

// LDAP
include (dirname(__FILE__) . "/adLDAP/lib/adLDAP/adLDAP.php");
echo "Connecting to LDAP\n\r\n";
try {
        $adldap = new adLDAP\adLDAP($ldap_options);
}
catch (adLDAPException $e) {
        exit();
}

$final_groups=get_classes($conn);
//print_r($final_groups);

// create and update all necessary groups
$groups=array();
$groups[]=array("name"=>"All Staff","query"=>"select person_pk,username from staff where roles like '%\"Staff\"%'");
$groups[]=array("name"=>"All Faculty","query"=>"select person_pk,username from staff where roles like '%\"Faculty\"%'");
$groups[]=array("name"=>"All Staff & Faculty","query"=>"select person_pk,username from staff where roles like '%\"Faculty\"%' OR roles like '%\"Staff\"%'");
$groups[]=array("name"=>"Faculty in Early Childhood Center","query"=>"select person_pk,username from staff where roles like '%\"Faculty\"%' AND school_level like '%Early Childhood Center%'");
$groups[]=array("name"=>"Faculty in Junior School","query"=>"select person_pk,username from staff where roles like '%\"Faculty\"%' AND school_level like '%Junior School%'");
$groups[]=array("name"=>"Faculty in Secondary 6-10","query"=>"select person_pk,username from staff where roles like '%\"Faculty\"%' AND school_level like '%Secondary 6-10%'");
$groups[]=array("name"=>"Faculty in Secondary 11-12","query"=>"select person_pk,username from staff where roles like '%\"Faculty\"%' AND school_level like '%Secondary 11-12%'");
$groups[]=array("name"=>"Students in Grade 6","query"=>"select person_pk,username from students where current_grade='Grade 6'");
$groups[]=array("name"=>"Students in Grade 7","query"=>"select person_pk,username from students where current_grade='Grade 7'");
$groups[]=array("name"=>"Students in Grade 8","query"=>"select person_pk,username from students where current_grade='Grade 8'");
$groups[]=array("name"=>"Students in Grade 9","query"=>"select person_pk,username from students where current_grade='Grade 9'");
$groups[]=array("name"=>"Students in Grade 10","query"=>"select person_pk,username from students where current_grade='Grade 10'");
$groups[]=array("name"=>"Students in Grade 11","query"=>"select person_pk,username from students where current_grade='Grade 11'");
$groups[]=array("name"=>"Students in Grade 12","query"=>"select person_pk,username from students where current_grade='Grade 12'");

foreach ($groups as $group) {
	$tmp_arr=array();
	$tmp_arr['name']=$group['name'];
        $result = mysqli_query($conn,$group['query']);
        if (mysqli_num_rows($result) > 0) {
               while($row = mysqli_fetch_assoc($result)){
			if ( isset($row['person_pk']) && isset($row['username'])&& $row['username']!="") $tmp_arr['members'][$row['person_pk']]=$row['username'];
			 if (! isset($row['person_pk']) ) print_r($row);
			}
		}
	$final_groups[]=$tmp_arr;
	}
        $ad_groups=$adldap->folder()->listing(array_reverse($container_for_auto_groups), adLDAP\adLDAP::ADLDAP_FOLDER, true, 'group');


//delete old groups
foreach ($ad_groups as $ad_group){
	if (! is_in_array($final_groups, "name", $ad_group['samaccountname'][0]) ) {
		$adldap->group()->delete($ad_group['samaccountname'][0]);
		echo "Group ".$ad_group['samaccountname'][0]." has been deleted\n";
		}
}

foreach ($final_groups as $key => $fgroup){
        if ( !isset( $fgroup['members'])) { continue; }


	//check if
	if (!checkGroup($adldap,$fgroup['name'])) {
		$attributes = array("group_name" => $fgroup['name'], "container" => $container_for_auto_groups , "description" => "Automatically created group", "mail" => str_replace(" ","_",$fgroup['name'])."@ishamburg.org");
		if (! $result = $adldap->group()->create($attributes)) echo "Coudn't create group\n";
	}

	//check membership - get array of current members and new array of members. Compare.
	$curmembers = $adldap->group()->members($fgroup['name']);
	$newmembers = $fgroup['members'];
	if ($curmembers == false) $curmembers=array();
	$to_add=array_udiff($newmembers,$curmembers,'strcasecmp');
	$to_del=array_udiff($curmembers,$newmembers,'strcasecmp');
	foreach ($to_add as $user){
		echo "Adding user $user to group ".$fgroup['name']."\n";
		$adldap->group()->addUser($fgroup['name'], $user);
		}
	foreach ($to_del as $user){
                echo "Deleting user $user from group ".$fgroup['name']."\n";
		$adldap->group()->removeUser($fgroup['name'], $user);
		}
}


unset($conn);
unset($final_groups);
unset($groups);


function checkGroup($adldap,$groupname){
	$tmp=$adldap->group()->info($groupname);
	if (isset($tmp[0] ) && is_array($tmp[0]) ) return true;
	else return false;
	}

function is_in_array($array, $key, $key_value){
      $within_array = false;
      foreach( $array as $k=>$v ){
        if( is_array($v) ){
            $within_array = is_in_array($v, $key, $key_value);
            if( $within_array == true ){
                break;
            }
        } else {
                if( $v == $key_value && $k == $key ){
                        $within_array = true;
                        break;
                }
        }
      }
      return $within_array;
}

?>
